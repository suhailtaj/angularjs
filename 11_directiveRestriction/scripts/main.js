(function() {
    'use strict'

    angular
        .module('superhero', [])
        .directive('superman', superman);

    // superman.$inject = ['$scope'];

    /* @ngInject */
    function superman() {
        // Usage:
        //
        // Creates:
        //
        var superman = {
            // bindToController: true,
            // controller: Controller,
            // controllerAs: 'vm',
            link: link,
            // To Ristrict as Attribute (default)
                // restrict: 'A',
            // To Ristrict as Element
                restrict: 'E',
            // To Ristrict as Class
                // restrict: 'C',
            // To Ristrict as Comment
                // restrict: 'M',
            // template: "<center><h2>Hello Super-Man</h2></center>"
            // scope: {},
        };
        return superman;

        function link(scope, element, attrs) {
            alert("Im Working ")


        }

    }

    /* @ngInject */
    // function Controller() {

})();